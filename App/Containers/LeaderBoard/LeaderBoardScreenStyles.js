import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
  },
  titleText: {
    ...Fonts.style.h3,
    fontFamily: Fonts.type.bold,
    paddingVertical: Metrics.doubleBaseMargin,
    color: Colors.snow,
    textAlign: 'center',
    marginTop: 30
  },
  centered: {
    alignItems: 'center',
    flex: 1
  },
  btnCircleGroup: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnItem: {
    width: 130,
    height: 35,
    backgroundColor: Colors.transparent,
    borderRadius: 25,
    borderColor: Colors.facebook,
    borderWidth: 2
  },
  leftItem: {
    marginRight: 10
  },
  clickedColor: {
    backgroundColor: Colors.pink,
    borderWidth: 0
  },
  btnText: {
    marginTop: 5,
    textAlign: 'center',
    color: Colors.snow,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
    backgroundColor: 'transparent'
  },
  topMembers: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  topMember: {

  },
  thirdMember: {
    marginRight: 0,
  },
  smallCircle: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: Colors.pink,
    alignItems: 'center',
    flex: 1,
    position: 'absolute',
    marginTop: 10,
    marginLeft: 8,
    zIndex: 1,
  },
  first: {
    backgroundColor: '#CCCC00',
    marginLeft: 4,
  },
  second: {
    backgroundColor: '#008000'
  },
  third: {
    backgroundColor: '#bf005f'
  },
  smallText: {
    marginTop: 7,
    textAlign: 'center',
    width: 20,
    color: Colors.snow,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.bold,
  },
  topAvatarSmallWrapper: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginTop: 15,
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: Colors.snow,
    overflow: 'hidden'
  },
  topAvatarIconImage: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  },
  firstTopMember: {
    marginTop: 0
  },
  topAvatarBigWrapper: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: Colors.snow,
    overflow: 'hidden'
  },
  nameText: {
    marginTop: 7,
    textAlign: 'center',
    color: Colors.snow,
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.bold,
  },
  longFirstName: {
    fontSize: Fonts.size.small,
  },
  firstName: {
    marginTop: 12
  },
  topPrice: {
    paddingHorizontal: 10,
    paddingVertical: 4,
    maxHeight: 25,
    borderRadius: 20,
    backgroundColor: '#317E9F',
    alignItems: 'center',
    flex: 1,
    marginTop: 7,
    alignSelf: 'center'
  },
  price: {
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.base,
    textAlign: 'center',
  },
  listContainer: {
    alignSelf: 'stretch',
    marginTop: 20,
  },
  rowContainer: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderColor: '#8E8E8E',
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16
  },
  leftPartWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  photoWrapper: {
    width: 40,
    height: 40,
    borderRadius: 20,
    overflow: 'hidden',
    marginLeft: 8
  },
  photo: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  },
  indexText: {
    color: Colors.snow,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.regular,
    textAlign: 'center'
  },
  rowNameText: {
    color: Colors.snow,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
    marginLeft: 10
  },
  rowPrice: {
    borderRadius: 15,
    backgroundColor: '#317E9F',
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  rowPriceText: {
    color: Colors.snow,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.regular,
    alignSelf: 'center',
  },
})
