import Database from '../api/Database'
import { AsyncStorage } from 'react-native'

class LeaderBoard extends Database {
  constructor() {
    super('leaderboard');
    AsyncStorage.getItem('currentUser')
    .then((currentUser) => {
      if (currentUser) {
        try {
          currentUserObj = JSON.parse(currentUser)
          this.user = currentUserObj
        } catch (error) {
          console.log('error constructor users model')
          console.log(error)
        }
      }
    })
  }

  init(userModel) {
    this.user = userModel
  }

  async getData() {
    const leaderBoardData = await super.getAllCollection()
    console.log('LeaderBoard getData')
    console.log(leaderBoardData)
    return leaderBoardData || {}
  }
}

export default LeaderBoardInstanse = new LeaderBoard()