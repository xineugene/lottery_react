import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors, Fonts } from '../../../Themes/'

import { height, width } from '../../../constants/config'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    paddingBottom: Metrics.baseMargin,
    alignItems: 'center'
  },
  contentContainerStyle: {
    paddingBottom: width(6)
  },
  titleText: {
    ...Fonts.style.h2,
    fontFamily: Fonts.type.bold,
    paddingVertical: Metrics.doubleBaseMargin,
    color: Colors.snow,
    textAlign: 'center',
    marginTop: width(20)
  },
  centered: {
    flex: 1,
    width: width(94),
    flexShrink: 1,
    flexWrap: 'wrap',
    alignSelf: 'center'
  },
  normalText: {
    flexDirection: 'row',
    fontSize: width(3.4),
    color: 'black',
    marginTop: 10,
    color: 'white'
  },
  smallTitle: {
    marginTop: 14,
    fontSize: width(4),
    fontWeight: '500',
    color: 'white'
  },
  bigTitle: {
    marginTop: 20,
    fontSize: width(4.8),
    fontWeight: '600',
    color: 'white'
  },
  bold: {
    fontWeight: '600',
    flexDirection: 'row',
    color: 'white'
  },
  list: {
    width: width(60),
    alignSelf: 'center'
  },
  underline: {
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: "#000"
  },
  imageWrapper: {
    width: '100%',
    height: width(90)
  },
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  }
})
