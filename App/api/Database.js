import firebase from 'react-native-firebase'
import moment from 'moment'
import { objectPattern } from 'babel-types';

const db = firebase.firestore()

export default class Database {
  constructor(modelName) {
    this.modelName = modelName
  }

  queryModel() {
    return db.collection(this.modelName)
  }

  getAllCollection() {
    return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .get()
        .then((querySnapshot) => {
          let collectionItems = [];
          querySnapshot.forEach((doc) => {
            collectionItems.push({
              ...doc.data(),
              id: doc.id
            })
          })
          resolve(collectionItems)
        })
        .catch((error)  =>{
          console.error('Error writing document: ', error);
          reject(error)
        });
    })
  }

  getModel(id) {
    if (id) return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .doc(id)  
        .get()
        .then((doc) => {
          console.log('doc.exists')
          console.log(doc.exists)
          if (doc.exists) {
            resolve(doc.data())
          } else {
            resolve()
          }
        })
        .catch((error) => {
          console.error('Error writing document: ', error);
          reject(error)
        })
    })
  }
  
  updateModel(id, keyValue) {
    return new Promise((resolve, reject) => {
    
      return db.collection(this.modelName)
        .doc(id)
        .update(keyValue)
        .then(() => {
          console.log('Document successfully written!');
          resolve()
        })
        .catch((error) => {
          console.error('Error writing document: ', error);
          reject(error)
        });
    })
  }

  updateModelWithReset(id, model) {
    return new Promise((resolve, reject) => {
    
      return db.collection(this.modelName)
        .doc(id)
        .set(model)
        .then(() => {
          console.log('Document successfully written!');
          resolve('success')
        })
        .catch((error) => {
          console.error('Error writing document: ', error);
          reject(error)
        });
    })
  }

  setModelById(id = new Date().getTime(), model) {
    return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .doc(id)
        .set(model)
        .then((docRef) => {
          console.log('Document written with ID: ', docRef);
          resolve(docRef)
        })
        .catch((error) => {
          console.error('Error adding document: ', error);
          reject(error)
        });
    })
  }  
      
  updateNestedObject(id, value, nestedObjId) {
    if (nestedObjId && id) return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .doc(id)  
        .update({[nestedObjId]: value})
        .then(() => {
          console.log('Document written');
          resolve(docRef)
        })
        .catch((error) => {
          console.error('Error adding document: ', error);
          reject(error)
        })
    })
  }
      
  pushNestedObject(id, value, nestedObjId) {
    if (nestedObjId && id) return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .doc(id)  
        .update({ [nestedObjId + '.' + moment().unix()]: value })
        .then(() => {
          console.log('Document written');
          resolve()
        })
        .catch((error) => {
          console.error('Error adding document: ', error);
          reject(error)
        })
    })
  }

  pushModel(model) {
    return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .add(model)
        .then((docRef) => {
          console.log('Document written with ID: ', docRef.id);
          resolve(docRef)
        })
        .catch((error) => {
          console.error('Error adding document: ', error);
          reject(error)
        })
    })
  }

  deleteModel(id) {
    return new Promise((resolve, reject) => {
      return db.collection(this.modelName)
        .doc(id)
        .delete()
        .then(() => {
          console.log('Document successfully deleted!');
          resolve()
        }).catch((error) => {
          console.error('Error removing document: ', error);
          reject(error)
        })
    })
  }
}