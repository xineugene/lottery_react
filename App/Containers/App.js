import '../Config'
import { Alert } from 'react-native'
import DebugConfig from '../Config/DebugConfig'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import RootContainer from './RootContainer'
import createStore from '../Redux'
import firebase from 'react-native-firebase'

// create our store
const store = createStore()

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */

class App extends Component {
  async componentDidMount() {
    this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
      console.log('gotNotifDisp')
      console.log(notification)
      notification.ios.setBadge(2);
      // firebase.notifications().displayNotification(notification)
      // Process your notification as required
      // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    });
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      console.log('gotNotif')
      console.log(notification)
      notification.ios.setBadge(2);
      firebase.notifications().displayNotification(notification)
        // Process your notification as required
    });
    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log('gotMessage')
      console.log(message)
      if (message && message._data) {
        const data = message._data
        const parsedData = data.custom_notification && JSON.parse(data.custom_notification)
        console.log(parsedData)
        const notification = new firebase.notifications.Notification()
          .setNotificationId('not' + message._messageId)
          .setTitle(parsedData && parsedData.title || data._title || 'Open Pinata app')
          .setBody(parsedData && parsedData.body || '');
        
        notification.ios.setBadge(2);
        console.log(notification)
        // firebase.notifications().displayNotification(notification)
      }
    });
    // this.notificationListener1 = FCM.on(FCMEvent.Notification, async (notif) => {
    //   console.log('gotNorr')
    //   console.log(notif)
    // if (!notif.local_notification) {
    //   FCM.presentLocalNotification({
    //     id: "UNIQ_ID_STRING",
    //     title: notif.aps.alert.title,
    //     body: notif.aps.alert.body,
    //     sound: "default",
    //     priority: "high",
    //     click_action: "ACTION",
    //     icon: "ic_launcher",
    //     show_in_foreground: true,
    //   });
    // }
    // });
    
  }

  componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
    this.messageListener();
    this.notificationBackgroundListener()
    this.notificationListener1()
  }
  
  render () {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}

// allow reactotron overlay for fast design in dev mode
export default DebugConfig.useReactotron
  ? console.tron.overlay(App)
  : App
