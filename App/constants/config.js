
import { Dimensions, Platform, StatusBar, StyleSheet } from 'react-native'
import firebase from 'react-native-firebase';
import moment from 'moment'

export const FBApp = firebase.app()

const window = Dimensions.get('window');

export const overlay = StyleSheet.absoluteFillObject

export const isIphoneX = Platform.OS === 'ios' && (window.height === 812 || window.width === 812)

export const statusBarHeight = Platform.OS == 'ios' ? isIphoneX ? 44 : 20 : StatusBar.currentHeight

export const width = (pers) => pers / 100 * window.width
export const height = (pers) => (pers / 100 * window.height) - statusBarHeight

export const isSmallScreen = window.height < 890


export const widthForOr = (initialOrientationKey, orientationkey) => initialOrientationKey == 'PORTRAIT'
    ? orientationkey == 'PORTRAIT' ? width : height
    : orientationkey == 'LANDSCAPE' ? height : width


export const heightForOr = (initialOrientationKey, orientationkey) => initialOrientationKey == 'PORTRAIT'
  ? orientationkey == 'PORTRAIT' ? height : width
	: orientationkey == 'LANDSCAPE' ? width : height


export const demWidth = Dimensions.get('window').width
export const demHeight = Dimensions.get('window').height

export const placementIds = {
  ios: '406136686509668_441124859677517',
  android: '406136686509668_441125063010830'
}

export const defaultImageBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAYAAAA+s9J6AAAAAXNSR0IArs4c6QAADgBJREFUeAHtnVlPFUkYhtk3gWEcQQWXiEajxuiFN4IJXhH/4vwV70w03nihMWIwLnFBURyHTXZw6j2ZIoWeluo+3f0dOQ8JoU731/V+/VS9dJ9eqpqnp6f/buIHAhAwI9BipowwBCBQIYAJ6QgQMCaACY0bAHkIYEL6AASMCWBC4wZAHgKYkD4AAWMCmNC4AZCHACakD0DAmAAmNG4A5CGACekDEDAmgAmNGwB5CGBC+gAEjAlgQuMGQB4CmJA+AAFjApjQuAGQhwAmpA9AwJgAJjRuAOQhgAnpAxAwJoAJjRsAeQhgQvoABIwJYELjBkAeApiQPgABYwKY0LgBkIcAJqQPQMCYACY0bgDkIYAJ6QMQMCaACY0bAHkIYEL6AASMCWBC4wZAHgKYkD4AAWMCmNC4AZCHACakD0DAmAAmNG4A5CGACekDEDAmgAmNGwB5CGBC+gAEjAlgQuMGQB4CmJA+AAFjApjQuAGQhwAmpA9AwJgAJjRuAOQhgAnpAxAwJoAJjRsAeQhgQvoABIwJYELjBkAeApiQPgABYwKY0LgBkIcAJqQPQMCYACY0bgDkIYAJ6QMQMCaACY0bAHkIYEL6AASMCWBC4wZAHgKYkD4AAWMCmNC4AZCHACakD0DAmAAmNG4A5CGACekDEDAmgAmNGwB5CGBC+gAEjAlgQuMGQB4CmJA+AAFjApjQuAGQhwAmpA9AwJgAJjRuAOQhgAnpAxAwJoAJjRsAeQhgQvoABIwJYELjBkAeApiQPgABYwJtxvrIRxKYn59ve/v2bc/Xr1871tbWWv2vNu/q6tr2v4cPH944derUysDAwFZk1YQZE2ienp7+2zgH5BMIrKystDx9+rT/48eP3YuLi+0JYVUX9/f3bx4/fnz18uXLiz09PTtVg1hYFwQ4EtZFM+xNYn19vVnme/78ef/W1laz1nZ0dOwMDQ2tDQ8Pr3Z3d2/39vZuOXNta50za+vy8nLb6upq64cPH7o/f/7cJdPq9+XLl33nz59flBk7Ozu/K56f+iLAkbC+2qPp1atXPQ8fPjy8sbFR+b5+7Nix1QsXLiy5o9paW1vc/0xn3CZ39OxyZzl9s7Oz3dpFmfj69etfR0dHV+pslxs+HUxYJ11gZ2en6dGjR39MTU39oZT03e7atWv/uiPfei0puiNjp6v3T32XVD2XLl1acPUutLRwTa4WrnluiwnzpJmxrs3NzeZ79+79NTMz06Mqrl69+u+VK1eWMlZXdbMnT570PX78+E+tHBkZWbl58+Y/7e3tnJ5WpVXuQv4dlsv7JzUdAb0B3enm94mJic95G1CiqlN1S0Nml6a0+bEngAmN20CnoDKFzDE5OTl78uTJtaJSUt3S8EaUdlFa1BtPABPGs8o9Uhdh/HfA8fHxOfc9cDN3kR8qlMbY2NicFktbOfwQwseSCWDCkoF7Od2G0FVQfdZ3wCKPgF7T/3U389ekqc/KQbn4dfwtnwAmLJ95RVH3AXUbQldBi/gOuN9uSVPaykG57BfP+uIIYMLi2CbWrCdhdCNeAboNkRhY8AqvrVyUU8FyVJ9AAPAJYIpcrCOPnoTRjfha7wPWkqe0lYNy4WhYC8natsWEtfHLtLWeBdWGehImUwU5buRz8DnlWDVVRRLAhJGg8grT2xB6plOPkelRtLzqzVqPclAuykm5Za2H7bITwITZ2WXaUq8jaUO94eDu12WqI8+NlIMeDFedPrc866eu/Qlgwv0Z5Rrhn+E8evSo+VHQ75jezFDZ5+aX87ccApiwHM67KnoZVx/0OtLuQuOCz8XnZpxOw8ljwpKb3Hd0vQ9YsnSinM/F55YYyIpCCGDCQrAmV+o7un8hNzmyvDU+F59becooiQAmpB9AwJgAJiy5ATQgkyQ1JEXJ0olyPhefW2IgKwohgAkLwZpcqe/oGhMmOarcNT4Xn1u56qhhwpL7gO/oGpSpZOlEOZ+Lzy0xkBWFEMCEhWBNrlRvLmjtp0+fupKjyl2jEdqk6HMrVx01TFhyH9DAvJLUs5oaFc36RzloiETl4XOzzqnR9DFhyS2ukbE1MK/e49OwhCXL/ySnHJSLcmLU7p/wlLIAE5aCea+InhvVEo0LundN+Z98Dj6n8jNAERMa9AGNhq3BljQwr8YFNUihIilt5aBclJNVHo2uiwkNeoDmhtDQ9JLWwLwGKVQkvbZyUU5WeTS6LiY06gE68ug9Pr25oIF5y05DmtJWDhwFy6a/Vw8T7uVR2idNzqK5ISSokbHfvXtX2kUaafnRuJUDE8WU1uxVhTBhVSzlLNTkLJobQmr3798fdEemVNOfZclSGtLSttJmgpgsFPPdBhPmyzN1bZqcRXNDaLClO3fuHCvyiKi6pSEtaUo7dcJskDsBTJg70nQVanYkTc7ijXj37t2hIr4jqk7V7Q0oTWZmStdWRUUzK1NRZFPWy9RoKYEdoHBMWGeNySShddYgJaSDCUuAnFYiabpsPdWiAaJ+NV22HgzXc6l6FE26uhHPdNlpW6DceExYLu9UahqaXiNjy1QaFzTNxnoWVKbVPUBuxKchV34sJiyfeSZFDcyrcUF1g11jwfhfVab3AP2vXkfS2xA8jJ0Js8lGdfN2t8ne/0aiMpX75fnO36jNYlPlFkUsKeIgUBABTFgQWKqFQCwBTBhLijgIFEQAExYElmohEEsAE8aSIg4CBRHAhAWBpVoIxBLAhLGkiINAQQQwYUFgqRYCsQQwYSwp4iBQEAFMWBBYqoVALAFMGEuKOAgURAATFgSWaiEQSwATxpIiDgIFEcCEBYGlWgjEEsCEsaSIg0BBBDBhQWCpFgKxBHipN5ZUyjiNnjY3N9eht+E1HbWbDbfNvw2vdfX2o+EP/dv5bgybrd7e3i29oT84OLjB0IjFthYmzJmvRrh+9uxZn8aFkelyrr6w6vSPwY1p06pfJ9IhITdtWr+MqbFqLl68uOSGztgsLIEGrpgxZnJq/KWlpVY3y9HAmzdvDvkq3QBL28PDwytHjhxZV1m/hw4d2m5vb//uY+rl7+bmZvO3b98qJpQRv3z50ummTuv535SVNE+fPv3Njdo939fXt10veR+EPDBhDq3ozNc/NTU1oKOJTt3OnDmzfO7cuWWdyuVQvWkVOqV+8eJF7+vXr3v9/rk5LOadGRnvJqeWwYQ1gNTRw02u8tf79+97VM2JEyc0v8P8QRzpTKO96Ugf7uv4+Pg/9XhUr6FJTTbFhBmx6/TTze0w6DpnhwbYvXHjxhd3ulaZBjtjlb/FZu50u/vBgwdHNKeF+2ezMTExMcfpaW1Nxy2KDPx0BPQGdN/ztiYnJ2cbwYBCpf3U/mq/9Q9IHMQjA0Y2+Z8AJszQFXQKqg6ojnj79u3ZRrtqqP3VfnsjikcGjGyCCbP1AV2E0fcinYLeunVrznXE+rvpl23XUm2l/db+i4N4iEuqCgjeJcCRcBfF/gV9D9RVUEWOjY3NNdoR8EdC2n9x0HJxEZ8fY/i8PwFMuD+j3QhdHdRlel0FdU+TrO2uaOCCOIiHuIhPA6PIvOuYMBKdu3ndrhvxug+o2xCRmzVEmHiIi/iIU0PsdI47iQkjYbpHuPoUevbs2aWDeB8wEkPVMPEYHR1d0krPqWogC6sSwIRVsexdqFMtPcKlpc6Ey3vX8kkE9ISQ/oqTePETTwATRrCanZ3tdLPntuiSvHsOlIeYqzATF/ERJ/GqEsKiBAKYMAFMuHhmZqZbn0dGRg78EzHhfqctez6eV9rtGzUeE0a0/MLCQuVig3sgez0ivGFDPB/Pq2FBpNxxTBgBzL8X6F5D2ooIb9gQz8fzalgQKXccE0YA29jYqNyE7uzs5IrDL3h5Pp7XL0JZFRDAhAGMpKL/z64hH5JiWN7U5Pl4XjCJI4AJIzj5S+7uOcmI6MYN8Xw8r8YlkW7PMWE6XkRDIHcCmDB3pFQIgXQEMGE6XkRDIHcCmDB3pFQIgXQEMGE6XkRDIHcCmDB3pFQIgXQEMGE6XkRDIHcCmDB3pFQIgXQEuPscwWtoaIihLCI4KQRWkaCCMAb/DWBQhIAFAU5HLaijCYGAACYMYFCEgAUBTGhBHU0IBAQwYQCDIgQsCGBCC+poQiAggAkDGBQhYEEAE1pQRxMCAQFMGMCgCAELApjQgjqaEAgIYMIABkUIWBDAhBbU0YRAQAATBjAoQsCCACa0oI4mBAICmDCAQRECFgQwoQV1NCEQEMCEAQyKELAggAktqKMJgYAAJgxgUISABQFMaEEdTQgEBDBhAIMiBCwIYEIL6mhCICCACQMYFCFgQQATWlBHEwIBAUwYwKAIAQsCmNCCOpoQCAhgwgAGRQhYEMCEFtTRhEBAABMGMChCwIIAJrSgjiYEAgKYMIBBEQIWBDChBXU0IRAQwIQBDIoQsCCACS2oowmBgAAmDGBQhIAFAUxoQR1NCAQEMGEAgyIELAhgQgvqaEIgIIAJAxgUIWBBABNaUEcTAgEBTBjAoAgBCwKY0II6mhAICGDCAAZFCFgQwIQW1NGEQEAAEwYwKELAggAmtKCOJgQCApgwgEERAhYEMKEFdTQhEBDAhAEMihCwIIAJLaijCYGAACYMYFCEgAUBTGhBHU0IBAQwYQCDIgQsCGBCC+poQiAggAkDGBQhYEEAE1pQRxMCAQFMGMCgCAELApjQgjqaEAgIYMIABkUIWBDAhBbU0YRAQAATBjAoQsCCACa0oI4mBAICmDCAQRECFgQwoQV1NCEQEMCEAQyKELAggAktqKMJgYAAJgxgUISABYH/AM5ONA7AQBIMAAAAAElFTkSuQmCC'