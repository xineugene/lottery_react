import React, { Component } from 'react'
import { ScrollView, TouchableOpacity, Text, Image, View } from 'react-native'
import { Images } from '../../../Themes'

// Styles
import styles from './FAQScreenStyles'

export default class FAQScreen extends Component {
  render () {
    const { navigate } = this.props.navigation;
    const { navigation } = this.props;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.backgroundImage}>
          <ScrollView style={styles.emptyBackground} contentContainerStyle={styles.contentContainerStyle}>
            <TouchableOpacity style={styles.backButton}  onPress={() => navigation.goBack()}>
              <Image source={Images.backButton}/>
            </TouchableOpacity>
            <View style={styles.centered}>
              <Text style={styles.titleText}>
                OFFICIAL CONTEST RULES
              </Text>
              <Text style={styles.smallTitle}>
                Kastner Management, LLC
              </Text>
              <Text style={styles.smallTitle}>
                “Piñata” Sweepstakes
              </Text>
              <Text style={styles.bigTitle}>
                OFFICIAL RULES  
              </Text>
              <Text style={styles.smallTitle}>
                NO AD VIEWING NECESSARY TO ENTER OR WIN. A VIEW WILL NOT IMPROVE YOUR CHANCES OF WINNING.  
              </Text>
              <Text style={styles.normalText}>
                Kastner Management “Piñata” Sweepstakes (the “Sweepstakes”) is intended for all entrants that agree to abide by and be bound by these Official Rules and the decisions of the Sponsor, which are final and binding in all matters relating to the Sweepstakes. Winning the Grand Prize (as described below) is contingent upon fulfilling all requirements set forth herein.
              </Text>  
              <Text style={styles.normalText}>
                <Text style={styles.bold}>SPONSOR:​</Text> The Sweepstakes is sponsored by Kastner Management, LLC., 782 N 800 E #14, Provo, UT 84606.  
              </Text>  
              <Text style={styles.normalText}>
                <Text style={styles.bold}>ELIGIBILITY:​​</Text> Subject to these Official Rules, the Sweepstake is open to anyone in the world who is at least 17 years old as of the date of entry. Employees of Sponsor and its respective parents, affiliates, subsidiaries, and advertising and promotion agencies and any other entity involved in the development or administration of this Contest, and its immediate family members or household members are not eligible to participate in or win the Contest. THE CONTEST IS VOID WHERE PROHIBITED OR RESTRICTED BY LAW OR WHERE BONDING, REGISTRATION, OR OTHER REQUIREMENTS WOULD BE REQUIRED BUT HAVE NOT BEEN MET, OR WHERE THE METHODS OF ENTRY SET FORTH BELOW WOULD BE DEEMED CONSIDERATION. ALL APPLICABLE FEDERAL, STATE AND LOCAL LAWS APPLY.
              </Text>  
              <Text style={styles.normalText}>
                <Text style={styles.bold}>TWO WAYS TO ENTER:</Text>​ The Sweepstakes opens at 9:00:00 a.m. Eastern Time (“ET”) on Saturday, and closes at 8:59:59 p.m. ET on the following Wednesday, then the Sweepstakes begins opens at 9:00:00 a.m. Eastern Time (“ET”) on Wednesday, and closes at 8:59:59 p.m. ET on Saturday. This process continues in said fashion (“Sweepstakes Period”). The Sponsor’s computer is the official time-keeping device for the Sweepstakes. A live broadcast will start once a Sweepstakes Period closes to reveal and announce any winners.
              </Text>  
              <Text style={styles.normalText}>
                <Text style={styles.bold}>1). Online Registration:</Text>​​ No purchase or payment is necessary to participate in the Contest. Enter the Contest by completing each of the following steps:
              </Text>  
              <View style={styles.list}>
                <Text style={styles.normalText}>
                  <Text style={styles.bold}>●</Text>​​ Download or open the Piñata Application downloaded through the Apple Store on your mobile device, and either (a) if you already have a Piñata account, log into your existing Piñata account, or (b) if you do not already have a Piñata account, sign up for a Piñata account by logging in through a register Facebook account if you plan to let advertisers play meaningful advertisements or with a verified phone number, if you wish to withhold personal information.
                </Text>
                <Text style={styles.normalText}>
                  <Text style={styles.bold}>●</Text>​​ Click “Watch.” Piñata will then automatically match you with two (2) 30-second advertisements. You may receive a notification when the live show begins.
                </Text>
                <Text style={styles.normalText}>
                  <Text style={styles.bold}>●</Text>​​ Watch at least two (2) 30-second advertisements to completion or click one (1) of the advertisements.
                </Text>
              </View> 
              <Text style={styles.normalText}>
                By downloading Piñata, you agree that any information provided by you or collected by Sponsor in connection with the Contest may be used by Sponsor in accordance with
                Piñata’s Privacy Policy located inside the application​ ​and Piñata’s Terms of Use located inside the application, and may be shared with Sponsor’s affiliated business entities. All
                information submitted to the PiñataApplication becomes the property of Sponsor.
                By downloading Piñata, you further agree that use of Piñatais subject to Piñata’s Terms of Use located inside the application and Privacy Policy located inside the application. Sponsor expressly reserves the right to disqualify any entries that it believes in good faith are generated by an automated means or scripts. Entries generated by script, macro or other automated means are void.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>2). Mail-in:</Text>​​ Eligible Entrants also have the option to obtain one (1) Sweepstakes entries via mail by legibly hand-printing, on a 3”x 5” card or paper, their full name, complete mailing address, phone number, and e-mail address, and mailing the card in a #10 envelope, with proper postage affixed, to: Kastner Management, LLC “Piñata” Sweepstakes, ​PO BOX​. You may submit as many postal entry requests as you wish, but each request must be mailed separately and will be used as an entry in the associated Sweepstakes Period the postal entry was received.
              </Text>
              <Text style={styles.smallTitle}>
                **NOTE: Mail-in entry card and envelope must be hand printed by the Entrant only. In addition, Entrants are not permitted to use any 3​rd​ party organization to assist with the entry process in any way (as determined by the ADMINISTRATOR). Each Envelope must be mailed individually. Bulk shipments of entries will not be accepted.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>BONUS DATES:</Text>​​ This applies to advertisements views ​<Text style={styles.underline}>and mail-in entries</Text>. <Text style={styles.bold}> ​Entries postmarked on random “double dates” will receive double entries.</Text>​​
              </Text> 
              <Text style={styles.normalText}>
                All required information must be provided to enter and to be eligible to win. Incomplete entries will be disqualified. Released Parties (defined below) are not responsible for: late, incomplete, incorrect, delayed, garbled, undelivered, misdirected, postage-due entries or mail. Photocopied, illegible, or mechanically reproduced entries are not eligible. All entries become the exclusive property of Sponsor and will not be acknowledged or returned. By participating, you consent for Sponsor to obtain, use, and transfer your name, address and other information for the purpose of administering this Sweepstakes and for other purposes as set forth below.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>GENERAL CONDITIONS:</Text>​​ ​If for any reason the operation or administration of this Sweepstakes is impaired or incapable of running as planned for any reason, including but not limited to (a) infection by computer virus, bugs, (b) tampering, unauthorized intervention, (c) fraud, (d) technical failures, or (e) any other causes beyond the control of the Sponsor which corrupt or affect the administration, security, fairness, integrity or proper conduct of this Sweepstakes, the Sponsor reserves the right at its sole discretion, to disqualify any individual who tampers with the entry process, and to cancel, terminate, modify or suspend the Sweepstakes in whole or in part, at any time, without notice and award the Grand Prize (defined below) using all non-suspect eligible entries received as of, or after (if applicable) this cancellation, termination, modification or suspension date, or in any manner that is fair and equitable and best conforms to the spirit of these Official Rules. Sponsor reserves the right, at its sole discretion, to disqualify any individual deemed to be (a) tampering or attempting to tamper with the entry process or the operation of the Sweepstakes or Sponsor’s Website; or (b) acting in violation of these Official Rules or in an unsportsmanlike or disruptive manner. CAUTION: ANY ATTEMPT TO DELIBERATELY DAMAGE ANY WEBSITE OR UNDERMINE THE LEGITIMATE OPERATION OF THE SWEEPSTAKES IS A VIOLATION OF CRIMINAL AND CIVIL LAWS AND SHOULD SUCH AN ATTEMPT BE MADE, THE SPONSOR RESERVES THE RIGHT TO SEEK DAMAGES OR OTHER REMEDIES FROM ANY SUCH PERSON (S) RESPONSIBLE FOR THE ATTEMPT TO THE FULLEST EXTENT PERMITTED BY LAW. Failure by the Sponsor to enforce any provision of these Official Rules shall not constitute a waiver of that provision. In the event of a dispute as to the identity of a Winner based on an email address, the winning entry will be declared by the authorized account holder of the email address submitted at time of entry. “Authorized account holder” is defined as the natural person who is assigned to an telephone number assigned by a provider, online service provider or other organization (e.g., business, educational, institution, etc.) that is responsible for assigning phone numbers for the domain associated with the submitted phone number.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>RELEASE AND LIMITATIONS OF LIABILITY:</Text>​​ By participating in the Sweepstakes, Entrants agree to release and hold harmless the Sponsor, the Administrator, their
                  respective parents, subsidiaries, affiliates, distributors, sales representatives, advertising and promotional agencies (collectively, the “Released Parties”) from and against any claim or cause of action arising out of participation in the Sweepstakes or receipt or use of any prize, including, but not limited to: (a) any technical errors that may prevent an Entrant from submitting an entry; (b) unauthorized human intervention in the Sweepstakes; (c) printing errors; (d) errors in the administration of the Sweepstakes or the processing of entries; or (e) injury, death, or damage to persons or property which may be caused, directly or indirectly, in whole or in part, from Entrant’s participation in the Sweepstakes or receipt of any prize. Released Parties assume no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration of, entries. Released Parties are not responsible for any problems or technical malfunction of any telephone network or telephone lines, computer online systems, servers, or providers, computer equipment, software, failure of any email or entry to be received by Sponsor on account of technical problems, human error or traffic congestion on the Internet or at any Website, or any combination thereof, including any injury or damage to Entrant’s or any other person’s computer relating to or resulting from participation in this Sweepstakes or downloading any materials in this Sweepstakes. Entrant further agrees that in any cause of action, the Released Parties’ liability will be limited to the cost of entering and participating in the Sweepstakes, and in no event shall the Released Parties be liable for attorney’s fees. Entrant waives the right to claim any damages whatsoever, including, but not limited to, punitive, consequential, direct, or indirect damages.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>DRAWING AND NOTIFICATION:</Text>​​ The drawing will be conducted by Pi​ña​ ta, whose decisions are final in all matters relating to this Sweepstakes. The drawing will occur in the 30 minute period before the opening of the Sweepstakes Period. Odds and combinations associated with winning a Prize are show below. The potential Grand Prize and other prize Winner(s) will be notified via the application.
              </Text>
              <View style={styles.imageWrapper}>
                <Image source={Images.faqImage} style={styles.image} />
              </View>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>PRIZES:</Text>​​ ​There can be a Grand Prize awarded at the end of each Sweepstakes Period. The Grand Prize is equal to $13,500 or the total accumulation in the pot (whichever is more). Monetary values for all other prizes are shown to the right of the winning combinations.
              </Text>
              <Text style={styles.normalText}>
                Prize to be fulfilled within 45 days of winners approval. The interior and exterior colors and all vehicle options (if any) are the Sponsor’s sole discretion. Pictures, display vehicles or other representations of any vehicle used in connection with this Sweepstakes are not depictions or promises of the Grand Prize which may differ substantially. The Grand Prize Winner must possess automobile insurance prior to receiving prize. Any costs associated with registration and licensing of the automobile are the sole responsibility of the Grand Prize Winner. No substitution or transfer of Prize will be permitted, except by the Sponsor, who reserves the right at its sole discretion to substitute a Prize with another Prize of equal or greater value. All other expenses associated with Prize acceptance not mentioned herein are the sole responsibility of the Grand Prize Winner.
              </Text>
              <Text style={styles.smallTitle}>
                NOTE #1: Any Winners over $1 must produce a photographed picture of themselves holding open the application, to receive their cash prize.
              </Text>
              <Text style={styles.smallTitle}>
                NOTE #2: Regardless of the accepted Prize (automobile or cash alternative), the Grand Prize Winner must travel to Provo, UT (the “Location”) to take possession of any Prize over $10,000. The Sponsor will ​provide​ one (1) standard round trip economy airfare ticket from a major commercial airport in the United States, near the Winner’s primary residence and the Location, as determined by Sponsor (Sponsor is not responsible for air transportation if the Winner resides within 150 miles of the Location). The Grand Prize Winner will also receive one (1) night hotel accommodations at a hotel determined by the Sponsor (room and tax only). Only international winners may choose to forgo the ceremony for travel reasons.
              </Text>
              <Text style={styles.smallTitle}>
                NOTE #3: The Grand Prize Winner understands and agrees that the Prize ceremony be videotaped/photographed. Rights to any Prize ceremony video/photo belong solely to the Sponsor and can be used in any media at their sole discretion.
              </Text>
              <Text style={styles.normalText}>
                Grand Prize will vary depending upon the selected prize, points of departure and destination and seasonal fluctuation of hotel rates and airfares. Any costs associated with registration and licensing of the automobile are the sole responsibility of the Grand Prize Winner. All other expenses not specifically mentioned herein, including but not limited to, airport taxes and fees, baggage fees, ground transportation not specifically delineated, food, beverages, additional hotel amenities or fees and gratuities, are the responsibility of the Grand Prize Winner. Sponsor is not responsible for any cancellations, delays, diversions or substitutions or any act or omissions whatsoever by the air transportation carriers, hotels or other transportation companies or any other persons providing any of these services and accommodations necessitated by same. Sponsors shall not be liable for any loss or damage to baggage and/or personal property.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>PRIZE CONDITIONS:</Text> By accepting the Prize, the Grand Prize Winner agrees to release and hold harmless the Released Parties, each of their related companies, and each of their respective officers, directors, employees, shareholders, and agents from and against any claim or cause of action arising out of participation in the Sweepstakes or receipt or use of the Prize. The potential winners of $1000 and up must sign and return to the Administrator, within seven (7) days of the date of notice or attempted notice is sent, an Affidavit of Eligibility, & Publicity Release in order to claim his/her prize. <Text style={styles.bold}>Note: The Affidavit sent will require that the Winner provide their Social Security Number to the Administrator, which will be used solely for tax reporting purposes.</Text> The Grand, Prize Winner will be responsible for all local, state, and federal
                taxes associated with the receipt of the Prize. The Grand Prize Winner must note that the value of their accepted Prize is taxable as income and an IRS Form 1099 will be filed in the name of the Winner for the value of the Prize and the Winner is solely responsible for all matters relating to the Prize after it is awarded. If a Prize or prize notification is returned as unclaimed or undeliverable to a potential Winner, if a potential Winner cannot be reached within three (3) business days from the first notification attempt, or if a potential Winner fails to return requisite document(s) within the specified time period, or if a potential Winner is not in compliance with these Official Rules, then such person shall be disqualified and, at Sponsor’s sole discretion, an alternate Winner may be selected.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>ENTRANT’S AGREEMENT:​</Text>​​ By accepting the Grand Prize, where permitted by law, each Winner grants to the Released Parties and those acting pursuant to the authority of Sponsor and the Released Parties (which grant will be confirmed in writing upon Sponsor’s request), the right to print, publish, broadcast and use worldwide IN ALL MEDIA without limitation at any time their full name, portrait, picture, voice, likeness and/or biographical information for advertising, trade and promotional purposes without further payment or additional consideration, and without review, approval or notification.
              </Text>
              <Text style={styles.normalText}>
                IN NO EVENT WILL RELEASED PARTIES BE RESPONSIBLE OR LIABLE FOR ANY DAMAGES OR LOSSES OF ANY KIND (INCLUDING WITHOUT LIMITATION, DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, OR PUNITIVE DAMAGES) ARISING OUT OF PARTICIPATION IN THIS SWEEPSTAKES OR THE ACCEPTANCE, POSSESSION, USE, OR MISUSE OF, OR ANY HARM RESULTING FROM THE ACCEPTANCE, POSSESSION, USE OR MISUSE OF THE PRIZE.
              </Text>
              <Text style={styles.normalText}>
                By participating, Entrants release and agree to hold harmless the Released Parties from any and all liability for any injuries, death or losses or damages to persons or property AS WELL AS CLAIMS/ACTIONS BASED ON PUBLICITY RIGHTS, DEFAMATION, AND/OR INVASION OF PRIVACY that may arise from participating in this Sweepstakes or its related activities or the acceptance, possession, use or misuse of, or any harm resulting from the acceptance, possession, use or misuse of the Prize. The Grand Prize Winner also acknowledges that Released Parties have neither made nor are in any manner responsible or liable for any warranty, representation or guarantee, express or implied, in fact, or in law, relative to the Prize.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>USE OF DATA:​</Text> ​​All information submitted by Entrants will be treated according to Sponsor’s privacy policy, available inside the application​.​ By participating in the Sweepstakes and providing your email address or any other applicable contact information, Entrants hereby agree to Sponsor’s collection and usage of their personal
                information and acknowledge that they have read and accepted Sponsor’s Privacy Policy.
              </Text>
              <Text style={styles.normalText}>
                <Text style={styles.bold}>DISPUTES​:</Text> By entering the Sweepstakes, Entrants agree that 1.) Any and all disputes, claims, and causes of action arising out of or connected with the Sweepstakes, or any prizes awarded, shall be resolved individually, without resort to any form of class action; 2.) Any and all claims, judgments, and awards shall be limited to actual out-of-pocket costs incurred, including costs associated with entering the Sweepstakes but in no event attorneys’ fees; and 3.) Under no circumstances will any Entrant be permitted to obtain any award for, and Entrant hereby waives all rights to claim, punitive, incidental or consequential damages and any and all rights to have damages multiplied or otherwise increased and any other damages, other than for actual out-of-pocket expenses, and any and all rights to have damages multiplied or otherwise increased. All issues and questions concerning the construction, validity, interpretation, and enforceability of these Official Rules, or the rights and obligations of the Entrants and Sponsor or the Released Parties in connection with the Sweepstakes, shall be governed by and construed in accordance with United States law the law of Utah. Any legal proceedings arising out of this Sweepstakes or relating to these Official Rules shall be instituted only in the federal or state courts of serving Provo, Utah, and Entrants, and the parties consent to jurisdiction therein with respect to any legal proceedings or disputes of whatever nature arising under or relating to these Official Rules. The invalidity or unenforceability of any provision of these Official Rules shall not affect the validity or enforceability of any other provision. In the event that any provision is determined to be invalid or otherwise unenforceable or illegal, these Official Rules shall otherwise remain in effect and be construed in accordance with their terms as if the invalid or illegal provision were not contained herein.
              </Text>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}
