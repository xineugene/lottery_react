export const numberWithCommas = (x) => {
  if (typeof x == 'string') x = Number(x)
  return x.toFixed(2) && x.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")  || ''
}