import Database from '../api/Database'
import { AsyncStorage } from 'react-native'

class Users extends Database {
  constructor() {
    super('users');
    AsyncStorage.getItem('currentUser')
    .then((currentUser) => {
      if (currentUser) {
        try {
          currentUserObj = JSON.parse(currentUser)
          this.user = currentUserObj
          console.log('c213urrentUserObj')
          console.log(currentUserObj)
        } catch (error) {
          console.log('error constructor users model')
          console.log(error)
        }
      }
    })
  }

  getUser() {
    return this.user
  }

  init(userModel) {
    this.user = userModel
  }
  
  addUser(userModel) {
    return super.setModelById(this.user.uid, userModel)
  }

  getUserInfo() {
    return super.getModel(this.user.uid)
  }

  modifyUser(keyValue) {
    console.log(this.user)
    console.log(keyValue)
    return super.getModel(this.user.uid)
      .then((result) => {
        console.log(result)
        console.log(keyValue)
        return super.updateModel(this.user.uid, keyValue)
      })
  }

  addTicket(ticketModel) {
    return super.pushNestedObject(this.user.uid, ticketModel, 'tickets')
  }

  deleteTicket(ticketId) {
    return super.updateNestedObject(this.user.uid, 'tickets' + '.' + ticketId)
  }

  async getTickets() {
    const userModel = await super.getModel(this.user.uid)
    console.log('userModel')
    console.log(userModel)
    return userModel && userModel.tickets || {}
  }
}

export default UsersInstance = new Users()