import React, { Component } from 'react'
import { ScrollView, TouchableOpacity, Text, Image, View, ListView, Alert, FlatList } from 'react-native'
import { Images } from '../../Themes'

import { numberWithCommas } from '../../utils'

// Styles
import styles from './LeaderBoardScreenStyles'

import LeaderBoardInstanse from '../../models/LeaderBoard'

const users = [
  { index: '1', username: 'test1', price: '$200', },
  { index: '2', username: 'test2', price: '$223', },
  { index: '3', username: 'test3', price: '$390', },
  { index: '4', username: 'test4', price: '$223', },
  { index: '5', username: 'test5', price: '$252', },
  { index: '6', username: 'test6', price: '$262', },
  { index: '7', username: 'test7', price: '$278', },
  { index: '8', username: 'test8', price: '$225', },
  { index: '9', username: 'test9', price: '$152', },
  { index: '10', username: 'test10', price: '$380', },
  { index: '11', username: 'test11', price: '$420', },
  { index: '12', username: 'test12', price: '$526', },
]

class Row extends Component {
  render() {
    const { item, idx, sortType } = this.props
    const { fName, allAmount, avatar, email, id, lName, lastWeek} = item
    return (
      <View style={styles.rowContainer}>
        <View style={styles.leftPartWrapper}>  
          <Text style={styles.indexText}>{idx+1}</Text>
          <View style={styles.photoWrapper}>
            <Image source={avatar && {uri: avatar} || Images.profileIcon} style={styles.photo} />
          </View>  
          <Text style={styles.rowNameText}>{fName + ' ' + lName}</Text>
        </View>
        <View style={styles.rowPrice}>
          <Text style={styles.rowPriceText}>{'$' + numberWithCommas(item[sortType])}</Text>
        </View>
      </View>
    )
  }
}

export default class LeaderBoardScreen extends Component {
  constructor(props) {
    super(props)
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      isWeekClicked: false,
      isAllClicked: false,
      dataSource: ds.cloneWithRows(users),
      data: [],
      sortType: 'allAmount'
    };
  }
  
  componentDidMount() {
    this.fetchLeaderBoardData()
  }

  fetchLeaderBoardData = async () => {
    const { sortType } = this.state
    try {
      const leaderBoardData = await LeaderBoardInstanse.getData()
      const sortedLeaderBoard = leaderBoardData.sort((a, b) => Number(b[sortType]) - Number(a[sortType]))
      console.log('sortedLeaderBoard')
      console.log(sortedLeaderBoard)
      this.setState({data: sortedLeaderBoard})
    } catch (error) {
      console.log(error)
      Alert.alert('fetchLeaderBoardData' + JSON.stringify(error))
    }
  }

  makeSort = (sortType) => {
    this.setState({ sortType: sortType }, () => {
      this.fetchLeaderBoardData()
    })
  }

  keyExtractor = (item, index) => item.id + index

  renderItem = ({ item, index }) => {
    const { sortType } = this.state
    return <Row item={item} idx={index} sortType={sortType}/>
  }
  
  render () {
    const { navigation } = this.props;
    const { sortType, data } = this.state
    return (
      <View style={styles.mainContainer}>
        <View style={styles.backgroundImage}>
          <ScrollView style={styles.emptyBackground}>
            <TouchableOpacity style={styles.backButton}  onPress={() => navigation.goBack()}>
              <Image source={Images.backButton}/>
            </TouchableOpacity>
            <View style={styles.centered}>
              <Text style={styles.titleText}>
                Leaderboard
              </Text>
              {/* Button Group */}
              <View style={styles.btnCircleGroup}>
                <TouchableOpacity 
                  style={[styles.btnItem, styles.leftItem, sortType == 'lastWeek' && styles.clickedColor]} 
                  // onPress={() => this.setState({isWeekClicked: !this.state.isWeekClicked, isAllClicked: this.state.isWeekClicked})}>
                  onPress={() => this.makeSort('lastWeek')}>
                  <Text style={styles.btnText}>
                    This week
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                  style={[styles.btnItem, sortType == 'allAmount' && styles.clickedColor]}
                  // onPress={() => this.setState({isAllClicked: !this.state.isAllClicked, isWeekClicked: this.state.isAllClicked})}>
                  onPress={() => this.makeSort('allAmount')}>
                  <Text style={styles.btnText}>
                    All time
                  </Text>
                </TouchableOpacity>
              </View>
              {/* Circle Group */}
              <View style={[styles.btnCircleGroup, styles.topMembers]}>
                {
                  data && data[1] ? 
                    <View style={styles.topMember}>
                      <View style={[styles.smallCircle, styles.second]}><Text style={styles.smallText}>2</Text></View>
                      <View style={styles.topAvatarSmallWrapper}>
                        <Image source={data[1].avatar && {uri: data[1].avatar} || Images.profileIcon} style={styles.topAvatarIconImage}/>
                      </View> 
                      <Text style={[styles.nameText, , data[1].fName && data[1].fName.length >= 8 && styles.longFirstName]}>{data[1].fName}</Text>
                      <View style={styles.topPrice}><Text style={styles.price}>{'$' + numberWithCommas(data[1][sortType])}</Text></View>
                    </View>  
                    : null
                }  
                {
                  data && data[0] ? 
                    <View style={[styles.topMember, styles.firstTopMember]}>
                      <View style={[styles.smallCircle, styles.first]}><Text style={styles.smallText}>1</Text></View>
                      <View style={styles.topAvatarBigWrapper}>
                        <Image source={data[0].avatar && {uri: data[0].avatar} || Images.profileIcon} style={styles.topAvatarIconImage}/>
                      </View> 
                      <Text style={[styles.nameText, styles.firstName, data[0].fName && data[0].fName.length >= 8 && styles.longFirstName]}>{data[0].fName}</Text>
                      <View style={styles.topPrice}><Text style={styles.price}>{'$' + numberWithCommas(data[0][sortType])}</Text></View>
                    </View>
                    : null
                }
                {
                  data && data[2] ? 
                    <View style={[styles.topMember, styles.thirdMember]}>
                      <View style={[styles.smallCircle, styles.third]}><Text style={styles.smallText}>3</Text></View>
                      <View style={styles.topAvatarSmallWrapper}>
                        <Image source={data[2].avatar && {uri: data[2].avatar} || Images.profileIcon} style={styles.topAvatarIconImage}/>
                      </View>  
                      <Text style={[styles.nameText, , data[2].fName && data[2].fName.length >= 8 && styles.longFirstName]}>{data[2].fName}</Text>
                      <View style={styles.topPrice}><Text style={styles.price}>{'$' +numberWithCommas(data[2][sortType])}</Text></View>
                    </View>
                    : null
                }
              </View>
              <FlatList
                style={styles.listContainer}  
                data={data}
                extraData={this.state}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}/>
            </View>            
          </ScrollView>
        </View>
      </View>
    )
  }
}
