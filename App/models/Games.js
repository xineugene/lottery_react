import Database from '../api/Database'
import { AsyncStorage } from 'react-native'

class Games extends Database {
  constructor() {
    super('games');
    AsyncStorage.getItem('currentUser')
    .then((currentUser) => {
      if (currentUser) {
        try {
          currentUserObj = JSON.parse(currentUser)
          this.user = currentUserObj
        } catch (error) {
          console.log('error constructor users model')
          console.log(error)
        }
      }
    })
  }

  init(userModel) {
    this.user = userModel
  }

  async getGames() {
    const gameModels = await super.getAllCollection()
    console.log('gameModels')
    console.log(gameModels)
    return gameModels || {}
  }
}

export default GamesInstanse = new Games()