import React, { Component } from 'react'
import { TouchableOpacity, Text, Image, View, AsyncStorage } from 'react-native'
import Video from 'react-native-video'
import Orientation from 'react-native-orientation';
import firebase from 'react-native-firebase'
// Styles
import styles from './NotificationAllowScreenStyles'

import MainScreen from '../MainScreen/MainScreen'
import UsersInstanse from '../../models/Users'

import { Images } from '../../Themes'

export default class NotificationAllowScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paused: false,
      repeat: true
    }
  }
  
  componentDidMount() {
    // this locks the view to Portrait Mode
    Orientation.lockToPortrait();
  }

  goToMainScreen = async (enablePushes) => {
    const { navigation } = this.props
    this.setState({
      paused: true,
      repeat: false
    }, async () => {
      if (enablePushes) {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
          navigation.navigate('MainScreen')
        } else {
          let newGameReminder;
          try {
            const gotPermissions = await firebase.messaging().requestPermission();
            newGameReminder = true
          } catch (error) {
            console.log('error geting notif permission')
            newGameReminder = false
          }
          UsersInstanse.modifyUser({ newGameReminder: newGameReminder })
            .then(() => {
          })
          .catch((error) => {
            Alert.alert(error)
          })
          await AsyncStorage.setItem('newGameReminder', JSON.stringify(!newGameReminder))
          navigation.navigate('MainScreen')
        }
      } else {
        navigation.navigate('MainScreen')
      }
    })
  }

  render () {
    const { navigation } = this.props;
    const { paused, repeat } = this.state
    return (
      <View style={styles.mainContainer}>
        <View style={styles.backgroundVideo}>
          {
            !paused 
              ?  <Video
                  source={require('../../Images/bgVideo.mp4')}
                  rate={1.0}
                  volume={1.0}
                  muted={false}
                  paused={paused}
                  muted={paused}
                  resizeMode={'cover'}
                  repeat={repeat}
                  style={styles.backgroundVideo}
              /> 
              : null
          }
        </View>
        {/* <TouchableOpacity style={styles.backButton}  onPress={() => navigation.goBack()}>
          <Image source={Images.backButton}/>
        </TouchableOpacity> */}
        <View style={styles.centered}>
          <TouchableOpacity style={styles.NoBtn} onPress={() => this.goToMainScreen(false)}>
            <Text style={styles.NoText}>Nah</Text>
          </TouchableOpacity>
          <Text style={styles.questionText}>Get notified when is live?</Text>
          <TouchableOpacity style={styles.YepBtn} onPress={() => this.goToMainScreen(true)}>
              <Text style={styles.btnText}>Yep!</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
