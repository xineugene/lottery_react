import React, { Component } from 'react';
import { View, ScrollView, Text, Modal, StyleSheet, FlatList, TouchableWithoutFeedback, TouchableOpacity, Image } from 'react-native';
import moment from 'moment'

import styles from './MainScreenStyles'
import { Images } from '../../Themes'

export default class TicketsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTicketsKey: 'current'
    }
  }
  
  _keyExtractorTicketsList = (item, index) => 'ticket_' + (item.key || index)

  renderTicketItem = ({ item, index }) => {
    // , item.winning && {backgroundColor:'#f2c6d2'}
    return (
      <View style={styles.ticketItemWrapper}>
        <View style={[styles.ticketItemInner]}>
          <Text style={styles.ticketIndexText}>
            {index+1}
          </Text>
          <View style={styles.ticketDataWrapper}>
            {
              item.data && item.data.length && item.data.map((field, idx) => {
                return (
                  <View style={[styles.itemWrapper, idx == item.data.length - 1 && {backgroundColor: '#CF617F'}]}>
                    <Text style={styles.itemNumber}>
                      {field}
                    </Text>  
                  </View>  
                )
              })
            }
          </View>
          <View style={styles.ticketDateTextWrapper}>
            <Text style={styles.ticketDateText}>
              {moment.unix(item.date).format('DD-MM-YYYY')}
            </Text>
            <Text style={styles.gameIdText}>
              {'g#' + item.gameId}
            </Text>
          </View>
        </View>
      </View>
    )
  }

  onPressShowTicketsBtn = (showTicketsKey) => {
    this.setState({showTicketsKey: showTicketsKey})
  }

  render() {
    const { showModal, isBigScreen, data, triggerModal, lastGameId } = this.props
    const { showTicketsKey } = this.state
    return (
      <Modal
        visible={showModal}
        animationType={'slide'}
        onRequestClose={() => console.log('close ticket modal')}
        transparent={true}>
        <View style={styles.modalOverlay}>

          <View style={[
            styles.modalContainer, 
            styles.modalTicketListContainer,
            isBigScreen 
              ? {margin: 25, marginTop: 80} 
              : {margin: 15, marginTop: 40}
            ]}>

            <View style={styles.contentInner}>
              <TouchableOpacity style={styles.closeBtn} onPress={triggerModal}>
                <Image source={Images.closeBtn} resizeMode={'contain'} style={styles.closeImg}/>
              </TouchableOpacity>
              <View style={styles.ticketBtnsWrapper}>
                <TouchableOpacity onPress={() => this.onPressShowTicketsBtn('current')}>
                  <View style={[styles.showTicketsBtnWrapper, showTicketsKey == 'current' && styles.showTicketsBtnWrapperActive]}>
                    <Text style={[styles.showTicketsBtnText, showTicketsKey == 'current' && styles.showTicketsBtnTextActive]}>
                      This week
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.onPressShowTicketsBtn('all')}>
                  <View style={[styles.showTicketsBtnWrapper, showTicketsKey == 'all' && styles.showTicketsBtnWrapperActive]}>
                    <Text style={[styles.showTicketsBtnText, showTicketsKey == 'all' && styles.showTicketsBtnTextActive]}>
                      All
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <ScrollView style={styles.ticketsListWrapper}>
                <FlatList
                  style={styles.ticketList}
                  contentContainerStyle={styles.ticketListInner}
                  data={data.filter((ticketData) => {
                    if (showTicketsKey == 'all') {
                      return ticketData.gameId != lastGameId
                    } else if (showTicketsKey == 'current') {
                      return ticketData.gameId == lastGameId
                    }
                  })}
                  extraData={this.state}
                  keyExtractor={this._keyExtractorTicketsList}
                  renderItem={this.renderTicketItem}/>
              </ScrollView>
            </View>

          </View>

        </View>
      </Modal>
    );
  }
}